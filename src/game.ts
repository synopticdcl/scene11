const blueBox = new Entity()
blueBox.addComponent(new BoxShape)

const pivot = new Entity()
pivot.addComponent(new Transform({position: new Vector3(8, 2, 8)}))
engine.addEntity(pivot)

blueBox.addComponent(new Transform({ position: new Vector3(3, 2, 3)}))
blueBox.setParent(pivot)
engine.addEntity(blueBox)

class Rotation {
  update() {  
    const transform = pivot.getComponent(Transform)  
    transform.rotate(Vector3.Up(), 3) 
  }
}
engine.addSystem(new Rotation)
